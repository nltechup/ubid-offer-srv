FROM node:alpine as build
WORKDIR /app/
ADD ["tsconfig.json", "package.json", "package-lock.json", "./"]
RUN npm i --silent
ADD src/ src/
RUN node_modules/.bin/tsc
ADD src/config/seed.json bin/config/seed.json

FROM node:alpine
COPY --from=build /app/bin/ /app/bin/
COPY --from=build /app/node_modules/ /app/node_modules/
COPY --from=build /app/package.json /app/
EXPOSE 8084
WORKDIR /app/
ENTRYPOINT [ "npm", "start" ]