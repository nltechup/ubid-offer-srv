import { Injectable } from '@nestjs/common';
import { createConnection, ConnectionOptions, Connection, ObjectLiteral, FindManyOptions, DeleteWriteOpResultObject } from 'typeorm';
import * as winston from 'winston';
import { ObjectId } from 'mongodb';

@Injectable()
export class DatabaseService {
    private connection: Connection;

    constructor(private options: ConnectionOptions, private logger: winston.Logger) { }

    public async connect(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                if (!(this.connection && this.connection.isConnected)) {
                    this.logger.info(`Trying to connect using: ${JSON.stringify(this.options)}`);

                    this.connection = await createConnection(this.options);
                    this.logger.info(`Connected to ${this.options.database} database`);
                }

                resolve();
            } catch (err) {
                this.logger.error(`Could not connect to ${this.options.database}: ${JSON.stringify(err)}`);
                reject(`Cannot open db connection`);
            }
        });
    }

    public async save(collection: any, record: any) {
        await this.connect();

        const result = await this.connection.getMongoRepository(collection).save(record);
        return result;
    }

    public async aggregate(collection: any, pipeline: ObjectLiteral[], options?: any): Promise<any[]> {
        await this.connect();

        const result = await this.connection.getMongoRepository(collection).aggregate(pipeline).toArray();
        return result;
    }

    public async findByIds(collection: any, ids: any[], condition?: any): Promise<any[]> {
        await this.connect();

        condition = condition || {};

        condition.deleted = null;

        const options = {
            where: condition,
        };

        const mongoIds = ids.map(id => ObjectId.createFromHexString(id));

        const result = await this.connection.getMongoRepository(collection).findByIds(mongoIds, options);
        return result;
    }

    public async findOne(collection: any, condition: any): Promise<{}> {
        await this.connect();

        condition.deleted = null;

        const options = {
            where: condition,
        };

        const result = await this.connection.getMongoRepository(collection).findOne(options);
        return result;
    }

    public async findAllAndCount(collection: any): Promise<[any[], number]> {
        await this.connect();

        const where: any = {
            deleted: null,
        };

        const order: any = {
            updated_date: 'DESC',
        };

        const options: FindManyOptions = {
            where,
            order,
        };

        return await this.connection.getMongoRepository(collection).findAndCount(options);
    }

    public async find(collection: any, condition: any, sort?: any, skip?: number, take?: number): Promise<any[]> {
        await this.connect();

        condition.deleted = null;

        const options: FindManyOptions = {
            where: condition,
        };

        if (sort) {
            options.order = sort;
        }

        if (skip) {
            options.skip = skip;
        }

        if (take) {
            options.take = take;
        }

        return await this.connection.getMongoRepository(collection).find(options);
    }

    public async patchOne(collection: any, condition: ObjectLiteral, update: any, options?: any): Promise<any> {
        await this.connect();

        condition.deleted = null;
        update = { $set: update };

        const result = await this.connection.getMongoRepository(collection).findOneAndUpdate(condition, update, options);
        return result.value;
    }

    public async upsertManyByFilter(collection: any, records: any[], filter: any): Promise<any> {
        if (records && records.length) {
            await this.connect();

            const promises = records.map(record => this.connection.getMongoRepository(collection).updateOne(filter, record, { upsert: true }));

            return await Promise.all(promises);
        }
    }

    public async removeByFilter(collection: any, filter: any): Promise<DeleteWriteOpResultObject> {
        await this.connect();

        this.logger.info(`Deleting many by filter - from EndMonthHandler: ${JSON.stringify(filter)} ...`);

        const repo = this.connection.getMongoRepository(collection);
        return await repo.deleteMany(filter);
    }
}
