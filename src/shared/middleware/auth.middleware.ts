import { Injectable, NestMiddleware, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: () => void) {
        // check for a header named x-token
        if (req.headers['x-user-uuid']) {
            next();
        } else {
            res.status(HttpStatus.UNAUTHORIZED).end();
        }
    }
}
