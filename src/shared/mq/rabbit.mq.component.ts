import { Injectable } from '@nestjs/common';
import * as amqp from 'amqplib';
import * as winston from 'winston';

import { Listener } from '../../app/listeners/listener';

@Injectable()
export class RabbitMessageQueue {
    private conn: amqp.Connection;

    private channel: amqp.Channel;

    constructor(private options: any, private logger: winston.Logger) { }

    public async initializeConnection(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            let connectionAttempts: number = 1;
            let connected: boolean = false;

            while (connectionAttempts <= this.options.retryCount && ! connected) {
                try {
                    this.conn = await amqp.connect(this.options.url);
                    this.channel = await this.conn.createChannel();

                    const exchangeAssert = await this.channel.assertExchange(this.options.exchangeName, 'topic');

                    if (exchangeAssert.exchange) {
                        connected = true;
                        this.logger.info(`Successfully connected to RabbitMQ after ${connectionAttempts} tries.`);
                        resolve();
                    } else {
                        throw new Error(`Could not create ${this.options.exchangeName} exchange ...`);
                    }
                } catch (error) {
                    if (connectionAttempts < this.options.retryCount) {
                        this.logger.warn(`Attempt ${connectionAttempts} failed (error:${error}), waiting another ${this.options.retryTimeout} seconds ...`);

                        connectionAttempts ++;
                        await this.delay(parseInt(this.options.retryTimeout, 10) * 1000);
                    } else {
                        this.logger.error(`Failed to connect to RabbitMq`);
                        reject();
                    }
                }
            }
        });
    }

    private delay(ms: number): Promise<void> {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    public async listenToQueue(listener: Listener): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            const exchangeAssert = await this.channel.assertExchange(listener.exchangeName || this.options.exchangeName, 'topic');
            const queueAssert = await this.channel.assertQueue(listener.queueName);

            if (exchangeAssert.exchange && queueAssert.queue) {
                await this.channel.bindQueue(listener.queueName, listener.exchangeName || this.options.exchangeName, listener.patternString);
                await this.channel.consume(listener.queueName, (async (msg: amqp.Message) => {
                    const result = await listener.onMessageReceived(msg);
                    result ? this.channel.ack(msg) : this.channel.nack(msg);
                    resolve(result);
                }));
                resolve(true);
            } else {
                reject();
            }
        });
    }

    public async publishMessage(message: { exchange?: string, routingKey: string, content: any, options: amqp.Options.Publish }): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            let result = false;

            try {
                message.exchange = message.exchange || this.options.exchangeName;
                this.logger.info(`Trying to publish message to: ${message.exchange}, ${message.routingKey}, content: ${JSON.stringify(message.content)}, options: ${this.options}`);

                await this.ensureInfrastructure();
                result = this.channel.publish(message.exchange, message.routingKey, Buffer.from(JSON.stringify(message.content)), message.options);
                this.logger.info(`Successfully published message: `, message);

                resolve(result);
            } catch (err) {
                this.logger.error(`Error publishing message on ${message.routingKey}: ${err}`);
                reject(err);
            }
        });
    }

    public async sendToQueue(message: { queue: string, content: any, options: amqp.Options.Publish }): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                await this.channel.assertQueue(message.queue);
                this.channel.sendToQueue(message.queue, Buffer.from(JSON.stringify(message.content)), message.options);
                this.logger.info(`Successfully sent to queue: `, message);
                resolve(true);
            } catch (err) {
                this.logger.error(`Error sending message to queue:  ${message.queue}: ${err}`);
                reject(err);
            }
        });
    }

    public async ensureInfrastructure(): Promise<void> {
        await this.channel.assertExchange(this.options.exchangeName, 'topic');
    }
}
