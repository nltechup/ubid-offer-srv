import { Injectable } from '@nestjs/common';
import * as winston from 'winston';

import { DatabaseService, RabbitMessageQueue } from '../../shared';
import { OfferVM } from '../models/offer.view-model';
import { OfferModel } from '../models/offer.model';
import { OFFER_CREATED_ROUTING_KEY } from '../../common/constants';
import { OfferViews } from '../pipes/offer.view.pipe';
import { OfferStatus } from '../models/offer.statuses';
import { BidModel } from '../models/bid.model';
import { OfferWithBids } from '../models/offerWithBids.view-model';

@Injectable()
export class OffersService {
    private readonly logger: winston.Logger;

    constructor(private databaseService: DatabaseService, private mq: RabbitMessageQueue) { }

    public async getOffers(param: any): Promise<{ title: string, values: OfferVM[] }> {
        const result: { title: string, values: OfferVM[] } = { title: '', values: [] };
        let queryType: { mapTo: string, condition?: any, sort?: any, skip?: any, take?: any } = { mapTo: '', condition: null, sort: null, skip: null, take: null };

        if (param) {
            if (param.viewName) {
                queryType = this.getQueryType(param.viewName);
            } else {
                if (param.cat_uuid && param.cat_name) {
                    result.title = param.cat_name;
                    queryType.condition = { cat_uuid: param.cat_uuid };
                    queryType.mapTo = 'OfferVM';
                } else {
                    if (param.timestamp) {
                        const since = new Date(param.timestamp);
                        queryType.condition = { createdAt: { $gte: since } };
                        queryType.mapTo = 'OfferSyncModel';
                    }
                }
            }
        }

        const { mapTo, condition, sort, skip, take } = queryType;

        const raw: OfferModel[] = await this.databaseService.find(OfferModel, condition, sort, skip, take);

        if (raw && raw.length) {
            result.values.push(...automapper.map('OfferModel', mapTo, raw));
        }

        return result;
    }

    public async getOffersByCond(cond: any): Promise<OfferVM[]> {
        const result = [];
        const raw: OfferModel[] = await this.databaseService.find(OfferModel, cond);

        if (raw && raw.length) {
            result.push(...automapper.map('OfferModel', 'OfferVM', raw));
        }

        return result;
    }

    // tslint:disable-next-line:variable-name
    public async getUserOffers(user_uuid: string): Promise<OfferWithBids[]> {
        const results: OfferWithBids[] = [];
        const offers = [];
        const offerIds: string[] = [];

        // find all offers for the given user
        const raw: OfferModel[] = await this.databaseService.find(OfferModel, { user_uuid }, { end: 'ASC' });

        // construct the query
        if (raw && raw.length) {
            offerIds.push(...raw.map(r => r._id.toHexString()));
            offers.push(...automapper.map('OfferModel', 'OfferVM', raw));
        }

        const filter = {
            offer_uuid: {
                $in: [ ...offerIds ],
            },
        };

        // identify possible bids
        const bids: BidModel[] = await this.databaseService.find(BidModel, filter) || [];

        // match bids with offers
        offers.forEach((offer: OfferVM) => {
            let result: Partial<OfferWithBids> = {
                ...offer,
            };

            const bid = bids.find(b => b.offer_uuid === offer.id);

            if (bid) {
                result = {
                    ...result,
                    ...bid,
                };
            }

            results.push(result as OfferWithBids);
        });

        return results;
    }

    public async getOfferById(id: string): Promise<OfferVM> {
        const raw = await this.databaseService.findByIds(OfferModel, [id]) as OfferModel[];

        if (raw && raw.length) {
            return automapper.map('OfferModel', 'OfferVM', raw[0] || {});
        }
    }

    public async saveOffer(model: OfferModel): Promise<any> {
        model.status = OfferStatus.CREATED;
        const result = await this.databaseService.save(OfferModel, model);

        await this.mq.publishMessage({
            content: result,
            routingKey: OFFER_CREATED_ROUTING_KEY,
            options: {},
        });

        return result;
    }

    private getQueryType(viewType: OfferViews): { mapTo: string, condition?: any, sort?: any, skip?: number, take?: number } {
        switch (viewType) {
            case OfferViews.latest: {
                return {
                    condition: {},
                    sort: { createdAt: 'DESC' },
                    skip: 0,
                    take: 6,
                    mapTo: 'OfferVM',
                };
            }
            case OfferViews.expiring: {
                const now = new Date();
                return {
                    condition: { end: { $gte: now } },
                    sort: { end: 'ASC' },
                    skip: 0,
                    take: 5,
                    mapTo: 'OfferVM',
                };
            }
            default: {
                return null;
            }
        }
    }
}
