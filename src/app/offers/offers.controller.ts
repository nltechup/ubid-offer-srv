import { Controller, Get, Post, Body, Headers, UsePipes, Query, Param } from '@nestjs/common';
import * as winston from 'winston';

import { OffersService } from './offers.service';
import { OfferVM } from '../models/offer.view-model';
import { LoggingService } from '../../shared';
import { OfferPipe, OfferViewPipe, OfferLinkPipe, EndPipe } from '../pipes';
import { OfferCreateModel } from '../models/offer.create-model';
import { OfferModel } from '../models/offer.model';
import { OfferWithBids } from '../models/offerWithBids.view-model';

@Controller('offers')
export class OffersController {
    private readonly logger: winston.Logger;

    constructor(private offerService: OffersService, private loggingService: LoggingService) {
        this.logger = this.loggingService.getLogger();
    }

    @Get('/me')
    async getMyOffers(@Headers('x-user-uuid') userId: string): Promise<OfferWithBids[]> {
        try {
            return await this.offerService.getUserOffers(userId);
        } catch (err) {
            this.logger.error(`Error retrieving offers: ${err}`);

            throw new Error(err);
        }
    }

    @Get('/:offerId')
    async getOfferDetails(@Param('offerId') offerId: string): Promise<OfferVM> {
        try {
            return await this.offerService.getOfferById(offerId);
        } catch (err) {
            this.logger.error(`Error retrieving offer by id ${offerId}: ${err}`);

            throw new Error(err);
        }
    }

    @Get()
    async getOffers(@Query(new OfferViewPipe(), new OfferLinkPipe(), new EndPipe()) param: any): Promise<{ title: string, values: OfferVM[] }> {
        try {
            return await this.offerService.getOffers(param);
        } catch (err) {
            this.logger.error(`Error retrieving offers: ${err}`);

            throw new Error(err);
        }
    }

    @Post()
    @UsePipes(OfferPipe)
    async addOffer(@Headers('x-user-uuid') userId: string, @Body() offer: OfferCreateModel): Promise<void> {
        try {
            const model = new OfferModel(offer);
            model.user_uuid = userId;

            return await this.offerService.saveOffer(model);
        } catch (err) {
            this.logger.error(`Error creating offer: ${err}`);

            throw new Error(err);
        }
    }
}
