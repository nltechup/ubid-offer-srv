import * as winston from 'winston';

import { Listener } from './listener';
import { RabbitMessageQueue } from '../../shared';
import { BidsService } from '../bids/bids.service';
import { BidCreatedModel } from '../models/bid-created.model';

export class BidCreatedListener implements Listener {
    public exchangeName: string;
    constructor(private bidsService: BidsService, private mq: RabbitMessageQueue, private logger: winston.Logger) { }
    readonly patternString: string = 'bid.create';
    readonly queueName: string = 'OfferBidCreate';

    async onMessageReceived(msg: any): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            if (msg && msg.content) {
                this.logger.info(`Received message on ${this.queueName} queue`);

                try {
                    const rawModel: any = JSON.parse(msg.content.toString());
                    const model: BidCreatedModel = new BidCreatedModel(rawModel);

                    await this.bidsService.processBid(model);

                    this.logger.info(`Successfully processed bid: ${ model }.`);
                } catch (error) {
                    this.logger.error(`Error executing: ${error}`);
                } finally {
                    resolve(true);
                }
            } else {
                this.logger.error(`Error processing sync mq: ${JSON.stringify(msg)}`);
                reject(`Error processing sync mq: ${JSON.stringify(msg)}`);
            }
        });
    }

}
