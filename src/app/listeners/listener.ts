export abstract class Listener {
    abstract queueName: string;

    abstract patternString: string;

    abstract exchangeName: string;

    abstract async onMessageReceived(msg: any): Promise<boolean>;
}
