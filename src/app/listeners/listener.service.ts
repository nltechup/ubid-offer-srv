import { Injectable } from '@nestjs/common';
import * as winston from 'winston';

import { Listener } from './listener';
import { RabbitMessageQueue } from '../../shared/mq/rabbit.mq.component';
import { LoggingService } from '../../shared/logging/logging.service';
import { SyncOfferListener } from './sync.offer.listener';
import { OffersService } from '../offers/offers.service';
import { BidCreatedListener } from './bid.create.listener';
import { BidsService } from '../bids/bids.service';

@Injectable()
export class ListenerService {
    private listeners: Listener[] = [];
    private logger: winston.Logger;

    constructor(private mq: RabbitMessageQueue,
                private loggingService: LoggingService,
                private offerService: OffersService,
                private bidsService: BidsService,
                ) {
        this.logger = this.loggingService.getLogger();
        this.initialiseListeners();
    }

    // tslint:disable-next-line:no-empty
    private initialiseListeners(): void {
        this.listeners.push(new SyncOfferListener(this.offerService, this.mq, this.logger));
        this.listeners.push(new BidCreatedListener(this.bidsService, this.mq, this.logger));
    }

    public async listen(): Promise<void> {
        await Promise.all(this.listeners.map(async (listener) => await this.mq.listenToQueue(listener)));
    }
}
