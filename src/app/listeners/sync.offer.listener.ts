import * as winston from 'winston';

import { Listener } from './listener';
import { OffersService } from '../offers/offers.service';
import { RabbitMessageQueue } from '../../shared';

export class SyncOfferListener implements Listener {
    public exchangeName: string;
    constructor(private offerService: OffersService, private mq: RabbitMessageQueue, private logger: winston.Logger) { }
    readonly patternString: string = 'offer.sync.request';
    readonly queueName: string = 'SyncOffersQueue';

    async onMessageReceived(msg: any): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            if (msg && msg.content && msg.properties.headers.since) {
                this.logger.info(`Received message on ${this.queueName} queue`);

                const timestamp = msg.properties.headers.since;
                const routingKeyResponse = msg.properties.replyTo;

                try {
                    const queryResults = await this.offerService.getOffers({ timestamp });

                    if (queryResults && queryResults.values) {

                        const response: { exchange?: string, routingKey: string, content: any, options: any } = {
                            routingKey: routingKeyResponse,
                            content: queryResults.values,
                            options: {},
                        };

                        const result = await this.mq.publishMessage(response);
                    }

                    this.logger.info(`Successfully sent ${queryResults.values.length} offers ...`);
                } catch (err) {
                    this.logger.error(`Error executing: ${err}`);
                } finally {
                    resolve(true);
                }
            } else {
                this.logger.error(`Error processing sync mq: ${JSON.stringify(msg)}`);
                reject(`Error processing sync mq: ${JSON.stringify(msg)}`);
            }
        });
    }
}
