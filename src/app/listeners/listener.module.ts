import { Module } from '@nestjs/common';

import { ListenerService } from './listener.service';
import { OffersModule } from '../offers/offers.module';
import { BidsModule } from '../bids/bids.module';

@Module({
    imports: [ OffersModule, BidsModule ],
    providers: [ ListenerService ],
    exports: [ ListenerService ],
})
export class ListenersModule {}
