import { Injectable } from '@nestjs/common';
// import * as moment from 'moment';

import { DatabaseService, RabbitMessageQueue } from '../../shared';
import { BidModel } from '../models/bid.model';
import { BidCreatedModel } from '../models/bid-created.model';

@Injectable()
export class BidsService {
    constructor(private databaseService: DatabaseService, private mq: RabbitMessageQueue) { }

    public async getBids(offerId: string): Promise<BidModel> {
        return await this.databaseService.findOne(BidModel, { offer_uuid: offerId }) as BidModel;
    }

    public async processBid(bid: BidCreatedModel): Promise<void> {
        if (bid) {
            let model: BidModel = await this.getBids(bid.offer_uuid);

            if (! model) {
                model = new BidModel({ offer_uuid: bid.offer_uuid, maxPrice: bid.price, bidsCount: 1, bidsValue: bid.price * bid.qty});
            } else {
                if (model.maxPrice < bid.price) {
                    model.maxPrice = bid.price;
                }

                model.bidsCount ++;
                model.bidsValue += bid.price * bid.qty;
            }

            return await this.databaseService.save(BidModel, model);
        }
    }
}
