import { Entity, ObjectIdColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ObjectId } from 'mongodb';
import { MinLength, MaxLength } from 'class-validator';

@Entity('categories')
export class CategoryModel {
    @ObjectIdColumn()
    // tslint:disable-next-line:variable-name
    _id?: ObjectId;

    @Column()
    @MinLength(5, { message: 'Denumirea este prea scurta' })
    @MaxLength(50, { message: 'Denumirea este prea lunga' })
    name: string;

    @Column()
    link: string;

    @CreateDateColumn()
    createdAt?: Date;

    @UpdateDateColumn()
    updatedAt?: Date;

    @Column()
    deleted?: boolean;
}
