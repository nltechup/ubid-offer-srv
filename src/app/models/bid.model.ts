import { Entity, ObjectIdColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ObjectId } from 'mongodb';
import { BidCreatedModel } from './bid-created.model';

@Entity('bids')
export class BidModel {
    @ObjectIdColumn()
    // tslint:disable-next-line:variable-name
    _id?: ObjectId;

    @Column()
    // tslint:disable-next-line:variable-name
    offer_uuid: string;

    @Column()
    maxPrice: number;

    @Column()
    bidsCount: number;

    @Column()
    bidsValue: number;

    @CreateDateColumn()
    createdAt?: Date;

    @UpdateDateColumn()
    updatedAt?: Date;

    constructor(copyFrom: {
        offer_uuid?: string,
        maxPrice?: number,
        bidsCount?: number,
        bidsValue?: number,
    } = {}) {
        this.offer_uuid = copyFrom.offer_uuid;
        this.maxPrice = copyFrom.maxPrice;
        this.bidsCount = copyFrom.bidsCount;
        this.bidsValue = copyFrom.bidsValue;
    }
}
