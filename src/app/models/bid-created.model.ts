export class BidCreatedModel {
    // tslint:disable-next-line:variable-name
    public offer_uuid: string;

    public price: number;

    public qty: number;

    constructor(copyFrom: {
        id?: string,
        offer?: {
            id?: string,
        },
        price?: number,
        qty?: number,
    } = {}) {
        this.offer_uuid = (copyFrom.offer && copyFrom.offer.id) ? copyFrom.offer.id : '';
        this.price = copyFrom.price || 0;
        this.qty = copyFrom.qty || 0;
    }
}
