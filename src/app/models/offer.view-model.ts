export class OfferVM {
    public id: string;

    public category: string;

    public title: string;

    public description: string;

    public link: string;

    public photoUrl: string;

    public price: number;

    public end: Date;

    public expired: boolean;
}
