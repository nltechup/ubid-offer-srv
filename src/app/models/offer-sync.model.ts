export class OfferSyncModel {
    id: string;

    product: string;

    description: string;

    quantity: number;

    // tslint:disable-next-line:variable-name
    photo_url: string;

    price: number;

    end: Date;
}
