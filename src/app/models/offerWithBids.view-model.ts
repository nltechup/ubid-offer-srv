import { OfferVM } from './offer.view-model';

export class OfferWithBids extends OfferVM {
    public bidsCount: number;

    public bidsValue: number;

    public maxPrice: number;
}
