import { Entity, ObjectIdColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ObjectId } from 'mongodb';
import { IsDate, MinLength, MaxLength, Min, Max, IsUrl, MinDate, MaxDate, IsUUID, IsISO8601, IsBoolean } from 'class-validator';
import { Type } from 'class-transformer';
import { OfferStatus } from './offer.statuses';

@Entity('offers')
export class OfferModel {
    @ObjectIdColumn()
    // tslint:disable-next-line:variable-name
    _id?: ObjectId;

    @Column()
    // tslint:disable-next-line:variable-name
    user_uuid?: string;

    @Column()
    @IsUUID()
    // tslint:disable-next-line:variable-name
    cat_uuid: string;

    @Column()
    @MinLength(5, { message: 'Denumirea este prea scurta' })
    @MaxLength(50, { message: 'Denumirea este prea lunga' })
    product: string;

    @Column()
    @MinLength(5, { message: 'Descrierea este prea scurta' })
    @MaxLength(100, { message: 'Descrierea este prea lunga' })
    description: string;

    @Column()
    @Min(1, { message: 'Cantitatea este prea mica' })
    @Max(9999, { message: 'Cantitatea este prea mare' })
    quantity: number;

    @Column()
    @IsUrl({}, { message: 'Adresa web invalida' })
    // tslint:disable-next-line:variable-name
    photo_url: string;

    @Column()
    @Min(0, { message: 'Pretul este prea mic' })
    @Max(99999, { message: 'Pretul este prea mare' })
    price: number;

    @Column()
    @MinDate(new Date(), { message: 'Data trebuie sa fie in viitor' })
    @IsDate({ message: 'Data nu este valida' })
    @Type(() => Date)
    end: Date;

    @Column()
    status: OfferStatus;

    @CreateDateColumn()
    createdAt?: Date;

    @UpdateDateColumn()
    updatedAt?: Date;

    @Column()
    deleted?: boolean;

    constructor(copyFrom: {
        cat_uuid?: string,
        product?: string,
        description?: string,
        quantity?: number,
        photoUrl?: string,
        price?: number,
        end?: Date,
    } = {}) {
        this.cat_uuid = copyFrom.cat_uuid;
        this.product = copyFrom.product;
        this.description = copyFrom.description;
        this.quantity = copyFrom.quantity;
        this.photo_url = copyFrom.photoUrl;
        this.price = copyFrom.price;
        this.end = copyFrom.end;
    }
}
