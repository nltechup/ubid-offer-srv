export enum OfferStatus {
    // the offer was just saved, not yet processed by the bidding service
    CREATED = 'CREATED',

    // the offer was processed by the bidding service and can receive bids
    ACTIVE = 'ACTIVE',

    // in the process of settlement by the settlement service
    PROCESSING = 'PROCESSING',

    // there are some winning bids, decided by the settlement service
    SETTLED = 'SETTLED',

    // there are no winning bids, the offer has expired, set by the settlement service
    EXPIRED = 'EXPIRED',
}
