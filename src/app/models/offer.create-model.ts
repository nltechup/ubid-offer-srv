import { IsDate, MinLength, MaxLength, Min, Max, IsUrl, MinDate, IsMongoId } from 'class-validator';
import { Type } from 'class-transformer';

export class OfferCreateModel {
    @IsMongoId()
    // tslint:disable-next-line:variable-name
    cat_uuid: string;

    @MinLength(5, { message: 'Denumirea este prea scurta' })
    @MaxLength(50, { message: 'Denumirea este prea lunga' })
    product: string;

    @MinLength(5, { message: 'Descrierea este prea scurta' })
    @MaxLength(100, { message: 'Descrierea este prea lunga' })
    description: string;

    @Min(1, { message: 'Cantitatea este prea mica' })
    @Max(9999, { message: 'Cantitatea este prea mare' })
    quantity: number;

    @IsUrl({}, { message: 'Adresa web invalida' })
    // tslint:disable-next-line:variable-name
    photoUrl: string;

    @Min(0, { message: 'Pretul este prea mic' })
    @Max(99999, { message: 'Pretul este prea mare' })
    price: number;

    @MinDate(new Date(), { message: 'Data trebuie sa fie in viitor' })
    @IsDate({ message: 'Data nu este valida' })
    @Type(() => Date)
    end: Date;
}
