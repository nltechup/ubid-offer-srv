import { Controller, Get, Query } from '@nestjs/common';
import * as winston from 'winston';

import { CategoriesService } from './categories.service';
import { LoggingService } from '../../shared';
import { CategoryViewPipe } from '../pipes/category.view.pipe';
import { CategoryVM } from '../models/category.view-model';

@Controller('categories')
export class CategoriesController {
    private readonly logger: winston.Logger;
    constructor(private categoryService: CategoriesService, private loggingService: LoggingService) {
        this.logger = this.loggingService.getLogger();
    }

    @Get()
    async getCategories(@Query(new CategoryViewPipe()) view: any): Promise<CategoryVM[]> {
        try {
            return await this.categoryService.getCategories(view);
        } catch (err) {
            this.logger.error(`Error retrieving categories: ${err}`);

            throw new Error(err);
        }
    }
}
