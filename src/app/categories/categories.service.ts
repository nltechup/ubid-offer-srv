import { Injectable } from '@nestjs/common';

import { DatabaseService } from '../../shared';
import { CategoryModel } from '../models/category.model';
import { CategoryVM } from '../models/category.view-model';
import { CategoryViews } from '../pipes/category.view.pipe';

@Injectable()
export class CategoriesService {
    constructor(private databaseService: DatabaseService) {}

    public async getCategories(viewType: CategoryViews): Promise<CategoryVM[]> {
        const result = [];
        const queryType = this.getQueryType(viewType);

        if (queryType) {
            const { mapTo, condition, sort, skip, take } = queryType;

            const raw: CategoryModel[] = await this.databaseService.find(CategoryModel, condition, sort, skip, take);

            if (raw && raw.length) {
                result.push(... automapper.map('CategoryModel', mapTo, raw));
            }
        }

        return result;
    }

    public async saveCategory(model: CategoryModel): Promise<any> {
        return await this.databaseService.save(CategoryModel, model);
    }

    public async getCategoryById(id: string): Promise<CategoryModel> {
        const categories = await this.databaseService.findByIds(CategoryModel, [id]) as CategoryModel[];

        return (categories && categories.length) ? categories[0] : null;
    }

    private getQueryType(viewType: CategoryViews): { mapTo: string, condition: any, sort?: any, skip?: number, take?: number } {
        switch (viewType) {
            case CategoryViews.vm: {
                return {
                    condition: {},
                    sort: { name: 'ASC' },
                    mapTo: 'CategoryVM',
                };
            }
            case CategoryViews.offer: {
                return {
                    condition: {},
                    sort: { name: 'ASC' },
                    mapTo: 'CategoryOfferModel',
                };
            }
            default: {
                return null;
            }
        }
    }
}
