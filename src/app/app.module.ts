import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';

import { ListenersModule } from './listeners/listener.module';
import { ListenerService } from './listeners/listener.service';
import { SharedModule } from '../shared/shared.module';
import { LoggingService } from '../shared/logging/logging.service';
import { DatabaseService } from '../shared/database/database.service';
import { RabbitMessageQueue } from '../shared/mq/rabbit.mq.component';
import { AuthMiddleware } from '../shared/middleware/auth.middleware';
import { OffersModule } from './offers/offers.module';
import { initializeAutomapper, initializeCategories } from '../common/utils';
import { CategoriesModule } from './categories/categories.module';
import { CategoriesService } from './categories/categories.service';
import { OffersService } from './offers/offers.service';
import { BidsModule } from './bids/bids.module';

@Module({
    imports: [ SharedModule,
               ListenersModule,
               OffersModule,
               CategoriesModule,
               BidsModule,
    ],
})
export class AppModule implements NestModule {
    constructor(private loggingService: LoggingService,
                private databaseService: DatabaseService,
                private mqService: RabbitMessageQueue,
                private listenerService: ListenerService,
                private categoriesService: CategoriesService,
                private offersService: OffersService) {}
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AuthMiddleware).forRoutes({ path: 'offers', method: RequestMethod.POST });
        consumer.apply(AuthMiddleware).forRoutes({ path: 'offers/me', method: RequestMethod.GET });
    }

    async onModuleInit() {
        try {
            this.loggingService.getLogger().info(`Initializing ubid-offer-service ...`);

            await this.databaseService.connect();
            await this.mqService.initializeConnection();
            await this.listenerService.listen();

            initializeAutomapper();
            await initializeCategories(this.categoriesService, this.offersService);

            this.loggingService.getLogger().info(`Successfully initialised ubid-offer-service`);
        } catch (error) {
            this.loggingService.getLogger().error(`Error initializing ubid-offer-service: ${error}`);
        }
    }
}
