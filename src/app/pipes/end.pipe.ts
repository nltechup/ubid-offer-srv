import { Injectable, PipeTransform, ArgumentMetadata, HttpException, HttpStatus } from '@nestjs/common';

@Injectable()
export class EndPipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata) {
        if (value.viewName || value.cat_uuid ) {
            return value;
        }

        throw new HttpException(`Parameter ${JSON.stringify(value)} is incorrect`, HttpStatus.BAD_REQUEST);
    }
}
