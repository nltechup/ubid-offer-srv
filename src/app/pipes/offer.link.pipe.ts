import { Injectable, PipeTransform, ArgumentMetadata, HttpException, HttpStatus } from '@nestjs/common';
import * as path from 'path';

import { SeedCategories } from '../../common/utils';
import { SEED_FOLDER, SEED_FILE } from '../../common/constants';

@Injectable()
export class OfferLinkPipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata) {
        if (value && value.cat_link) {
            const seed: SeedCategories = require(path.join(SEED_FOLDER, SEED_FILE));

            if (seed && seed.categories) {
                const category = seed.categories.find(cat => cat.link === value.cat_link);

                if (category) {
                    value.cat_uuid = category._id;
                    value.cat_name = category.name;
                }
            }
        }

        return value;
    }
}
