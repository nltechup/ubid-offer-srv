import { PipeTransform, ArgumentMetadata, Injectable } from '@nestjs/common';
import { OfferCreateModel } from '../models/offer.create-model';

@Injectable()
export class OfferPipe implements PipeTransform {
    transform(value: OfferCreateModel, metadata: ArgumentMetadata) {
        value.end = new Date(value.end);

        return value;
    }
}
