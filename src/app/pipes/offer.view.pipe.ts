import { Injectable, PipeTransform, ArgumentMetadata, HttpException, HttpStatus } from '@nestjs/common';

export enum OfferViews {
    latest = 'LATEST',
    me = 'ME',
    expiring = 'ABOUT_TO_EXPIRE',
}

@Injectable()
export class OfferViewPipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata) {
        if (value && value.view) {
            const viewName = value.view as string;
            value.viewName = OfferViews[viewName.toLowerCase()];
        }

        return value;
    }
}
