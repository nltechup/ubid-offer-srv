import { Injectable, PipeTransform, ArgumentMetadata } from '@nestjs/common';

export enum CategoryViews {
    vm = 'VIEW-MODEL',
    offer = 'OFFER-MODEL',
}

@Injectable()
export class CategoryViewPipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata) {
        if (value.view) {
            const viewName = value.view as string;
            return CategoryViews[viewName.toLowerCase()];
        }

        return null;
    }
}
