import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value, { metatype }: ArgumentMetadata) {
    if (! metatype || ! this.toValidate(metatype)) {
        return value;
    }

    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    if (errors.length > 0) {
      const errMsg = [];

      errors.forEach(err => {
        const obj = Object.assign({}, err.constraints);

        errMsg.push(obj[Object.getOwnPropertyNames(obj)[0]]);
      });

      throw new BadRequestException(`Eroare la validare: ${errMsg.join('; ')}`);
    }

    return value;
  }

  private toValidate(metatype): boolean {
        const types = [String, Boolean, Number, Array, Object];
        return ! types.find((type) => metatype === type);
  }
}
