import * as path from 'path';

export const CONFIG_FOLDER = path.join(__dirname, '..', '..', 'configuration');
export const CONFIG_FILE = 'config.yml';
export const SEED_FOLDER = path.join(__dirname, '..', 'config');
export const SEED_FILE = 'seed.json';
export const MAX_PAGE_SIZE = 10;
export const OFFER_CREATED_ROUTING_KEY = `offer.create`;
export const OFFER_UPDATED_ROUTING_KEY = `offer.update`;
export const OFFER_DELETED_ROUTING_KEY = `offer.delete`;
