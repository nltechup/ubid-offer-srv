import 'automapper-ts';
import * as path from 'path';
import { ObjectId } from 'mongodb';

import { CategoriesService } from '../app/categories/categories.service';
import { OffersService } from '../app/offers/offers.service';
import { CategoryModel } from '../app/models/category.model';
import { SEED_FILE, SEED_FOLDER } from './constants';
import { OfferModel } from '../app/models/offer.model';
import { OfferStatus } from '../app/models/offer.statuses';

// tslint:disable-next-line:no-var-requires
const seed: SeedCategories = require(path.join(SEED_FOLDER, SEED_FILE));

export interface SeedCategories {
    categories: [{ _id: string, name: string, link: string }];
    offers: [{ cat_uuid: string, product: string, description: string, quantity: number, photo_url: string, end: string, price: number, user_uuid: string }];
}

export function initializeAutomapper() {
    automapper.initialize((config: AutoMapperJs.IConfiguration) => {
        config.createMap('OfferModel', 'OfferVM')
            .forMember('_id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.sourceObject._id.toHexString())
            .forMember('user_uuid', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('cat_uuid', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('category', (opts: AutoMapperJs.IMemberConfigurationOptions) => seed.categories.find((cat) => cat._id === opts.sourceObject.cat_uuid).name)
            .forMember('createdAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('updatedAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('photoUrl', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('photo_url'))
            .forMember('title', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('product'))
            .forMember('expired', (opts: AutoMapperJs.IMemberConfigurationOptions) => new Date() > new Date(opts.sourceObject.end));

        config.createMap('OfferModel', 'OfferSyncModel')
            .forMember('_id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.sourceObject._id.toHexString())
            .forMember('user_uuid', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('cat_uuid', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('createdAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('updatedAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore);

        config.createMap('CategoryModel', 'CategoryVM')
            .forMember('_id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.sourceObject._id.toHexString())
            .forMember('createdAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('updatedAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore);

        config.createMap('CategoryModel', 'CategoryOfferModel')
            .forMember('_id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.sourceObject._id.toHexString())
            .forMember('link', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('createdAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore)
            .forMember('updatedAt', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.ignore);
    });
}

export async function initializeCategories(categoriesService: CategoriesService, offersService: OffersService): Promise<void> {
    seed.categories.forEach(async (category: { _id: string, name: string, link: string }) => {
        const existCategories = await categoriesService.getCategoryById(category._id);

        if (! existCategories) {
            try {
                const categoryDb: CategoryModel = { _id: ObjectId.createFromHexString(category._id), name: category.name, link: category.link };
                await categoriesService.saveCategory(categoryDb);
            } catch (err) {
                throw new Error(err);
            }
        }
    });

    const existOffers = await offersService.getOffers({ viewName: 'LATEST'});

    if (existOffers.values.length === 0) {
        seed.offers.forEach(async (offer: { cat_uuid: string, product: string, description: string, quantity: number, photo_url: string, end: string, price: number, user_uuid: string }) => {
            try {
                const offerDb: OfferModel = {
                    cat_uuid: offer.cat_uuid,
                    product: offer.product,
                    description: offer.description,
                    quantity: offer.quantity,
                    photo_url: offer.photo_url,
                    price: offer.price,
                    user_uuid: offer.user_uuid,
                    end: new Date(offer.end),
                    status: OfferStatus.CREATED,
                };

                await offersService.saveOffer(offerDb);
            } catch (err) {
                throw new Error(err);
            }
        });
    }
}
